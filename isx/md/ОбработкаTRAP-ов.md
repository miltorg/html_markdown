[Ч115](http://вики-ч115.программирование-по-русски.рф/%d0%a7115)/

# ОбработкаTRAP-ов

Цель статьи - изучить TRAPы, чтобы уметь использовать их в программировании. Например, в среде разработки, если текст некорректен, использовать TRAP для сообщения
об этом.

## Элементы кода

Платформа.Traps.Mod - модуль Traps, платформо-зависимый.

* Traps.Show - показать исключение
* Traps.GetLastExceptionState - ?
* Traps..Exception - обработчик исключения (не экспортирован)
* Machine.InstallHandler - установить обработчик исключения (например, Traps..Exception)
* TrapWriters.TrapWriter - видимо, это объект, который печатает трапы в разные места, там можно и отключить вывод трапов.
* TrapWriters.WriterFactory = PROCEDURE (): Streams.Writer;
* TrapWriters.InstallTrapWriterFactory - печатать трапы ещё и в указанное место. Но есть и
* TrapWriters.UninstallTrapWriterFactory

## Objects.halt и Objects.haltUnbreakable

Я так примерно понял, что haltUnbreakable более жёстко убивает процесс. Его можно увидеть в ГПИ в инструменте Inspect/Objects.
В обоих случаях пытаемся выполнить FINALLY, но в haltUnbreakable их тоже выполняем как-то более агрессивно.

## Откуда берётся красный экран?

Он настроен в Configuration.XML

<br>
<Section name = "Autostart"><br>
<Setting name = "FileTrapWriter" value = "FileTrapWriter .Install "/><br>
<Setting name = "TrapWriter" value = "WMTrapWriter .Install "/>[[$[Get Code]]](http://%d0%b2%d0%b8%d0%ba%d0%b8-%d1%87115.%d0%bf%d1%80%d0%be%d0%b3%d1%80%d0%b0%d0%bc%d0%bc%d0%b8%d1%80%d0%be%d0%b2%d0%b0%d0%bd%d0%b8%d0%b5-%d0%bf%d0%be-%d1%80%d1%83%d1%81%d1%81%d0%ba%d0%b8.%d1%80%d1%84/%d0%a7115/%d0%9e%d0%b1%d1%80%d0%b0%d0%b1%d0%be%d1%82%d0%ba%d0%b0TRAP-%d0%be%d0%b2?action=sourceblock&num=1)

FileTrapWriter.Install делает так, что для каждого трапа создаётся файл Trap_....txt в рабочей директори (напр., WinAOS)
WMTrapWriter показывает красный экран.

Соответственно, выполнение

<br>
WMTrapWriter .Uninstall [[$[Get Code]]](http://%d0%b2%d0%b8%d0%ba%d0%b8-%d1%87115.%d0%bf%d1%80%d0%be%d0%b3%d1%80%d0%b0%d0%bc%d0%bc%d0%b8%d1%80%d0%be%d0%b2%d0%b0%d0%bd%d0%b8%d0%b5-%d0%bf%d0%be-%d1%80%d1%83%d1%81%d1%81%d0%ba%d0%b8.%d1%80%d1%84/%d0%a7115/%d0%9e%d0%b1%d1%80%d0%b0%d0%b1%d0%be%d1%82%d0%ba%d0%b0TRAP-%d0%be%d0%b2?action=sourceblock&num=2)

полностью отключает красный экран.

## Идеи

Иногда может захотеться <<молчаливое исключение>>, которое обрабатывается, но не показывается. Решение - в ветке silent-trap-7777, общая идея - в том, что для выбранного кода исключения 7777 никакое отображение не происходит.

diff --git "a /C :\\Users\\ASUS\\AppData\\Local\\Temp\\TortoiseGit\\Win32 .Traps -bc673e6 . 000 .Mod " "b /C :\\Users\\ASUS\\AppData\\Local\\Temp\\TortoiseGit\\Win32 .Traps -b6e7f36 . 000 .Mod "<br>
index f1dd4ceb8f ..e0db336ed7 100644<br>
---"a /C :\\Users\\ASUS\\AppData\\Local\\Temp\\TortoiseGit\\Win32 .Traps -bc673e6 . 000 .Mod "<br>
+++ "b /C :\\Users\\ASUS\\AppData\\Local\\Temp\\TortoiseGit\\Win32 .Traps -b6e7f36 . 000 .Mod "<br>
@@-339 ,7 +339 ,9 @@VAR<br>
*)<br>
traceTrap :=FALSE ;<br>
<br>
-Show (t , int , exc ,(* exc.halt # MAX(INTEGER)+1*)TRUE );(* Always show the trap info!*)<br>
+IF exchalt # 7777 (*Invisible trap*)THEN<br>
+Show (t , int , exc ,(* exc.halt # MAX(INTEGER)+1*)TRUE );(* Always show the trap info!*)<br>
+END ;<br>
<br>
IF exchalt =haltUnbreakable THEN Unbreakable (t , int , exc , handled )<br>
ELSIF ~traceTrap THEN HandleException (int , exc , handled ) [[$[Get Code]]](http://%d0%b2%d0%b8%d0%ba%d0%b8-%d1%87115.%d0%bf%d1%80%d0%be%d0%b3%d1%80%d0%b0%d0%bc%d0%bc%d0%b8%d1%80%d0%be%d0%b2%d0%b0%d0%bd%d0%b8%d0%b5-%d0%bf%d0%be-%d1%80%d1%83%d1%81%d1%81%d0%ba%d0%b8.%d1%80%d1%84/%d0%a7115/%d0%9e%d0%b1%d1%80%d0%b0%d0%b1%d0%be%d1%82%d0%ba%d0%b0TRAP-%d0%be%d0%b2?action=sourceblock&num=3)

См. также [https://forum.oberoncore.ru/viewtopic.php?f=22&t=6468](https://forum.oberoncore.ru/viewtopic.php?f=22&t=6468)
