[Ч115](http://вики-ч115.программирование-по-русски.рф/%d0%a7115)/

# ИнкрементнаяРазработкаWin32

Когда модуль компилируется, он кладётся в директорию Work (такая директория должна существовать внутри WinAos, её отсутствие приведёт к разным проблемам).
При открытии модуля, его файлы (GofW и SymW) сначала ищутся внутри WinAos/Work, а уже потом - внутри WinAos/obg.

См. также [Инкрементная разработка и модули](http://%d0%b2%d0%b8%d0%ba%d0%b8-%d1%87115.%d0%bf%d1%80%d0%be%d0%b3%d1%80%d0%b0%d0%bc%d0%bc%d0%b8%d1%80%d0%be%d0%b2%d0%b0%d0%bd%d0%b8%d0%b5-%d0%bf%d0%be-%d1%80%d1%83%d1%81%d1%81%d0%ba%d0%b8.%d1%80%d1%84/%d0%a7115/%d0%98%d0%bd%d0%ba%d1%80%d0%b5%d0%bc%d0%b5%d0%bd%d1%82%d0%bd%d0%b0%d1%8f%d0%a0%d0%b0%d0%b7%d1%80%d0%b0%d0%b1%d0%be%d1%82%d0%ba%d0%b0%d0%98%d0%9c%d0%be%d0%b4%d1%83%d0%bb%d0%b8)
