[Ч115](http://вики-ч115.программирование-по-русски.рф/%d0%a7115)/

# ПолучениеWriterВыходногоПотокаДляKernelLog

Иногда в качестве параметра нужен Writer (поток вывода), а мы хотим, чтобы вывод происходил в KernelLog. Тогда смотрим в конец модуля Debugging
и видим примерно такой образец кода:

VAR writer : Streams .Writer ;<br>
BEGIN<br>
Streams .OpenWriter (writer , KernelLog .Send );[[$[Get Code]]](http://%d0%b2%d0%b8%d0%ba%d0%b8-%d1%87115.%d0%bf%d1%80%d0%be%d0%b3%d1%80%d0%b0%d0%bc%d0%bc%d0%b8%d1%80%d0%be%d0%b2%d0%b0%d0%bd%d0%b8%d0%b5-%d0%bf%d0%be-%d1%80%d1%83%d1%81%d1%81%d0%ba%d0%b8.%d1%80%d1%84/%d0%a7115/%d0%9f%d0%be%d0%bb%d1%83%d1%87%d0%b5%d0%bd%d0%b8%d0%b5Writer%d0%92%d1%8b%d1%85%d0%be%d0%b4%d0%bd%d0%be%d0%b3%d0%be%d0%9f%d0%be%d1%82%d0%be%d0%ba%d0%b0%d0%94%d0%bb%d1%8fKernelLog?action=sourceblock&num=1)

Теперь writer - это открытый Streams.Writer. Как он закрывается - то мне неведомо (в Обероне вроде принято всё бросать).
Но нужно вызывать writer.Update при параллельной записи из другого журнала. Также нужно вызывать KernelLog.Update.
