# Получение Writer выходного потока для KernelLog

Иногда в качестве параметра нужен Writer (поток вывода), а мы хотим, чтобы вывод происходил в KernelLog.
Тогда смотрим в конец модуля Debugging и видим примерно такой образец кода:
```
VAR writer : Streams .Writer ;  
BEGIN  
Streams .OpenWriter (writer , KernelLog .Send );  
```

Теперь writer - это открытый Streams.Writer. Как он закрывается - то мне неведомо (в Обероне вроде принято всё бросать).
Но нужно вызывать writer.Update при параллельной записи из другого журнала. Также нужно вызывать KernelLog.Update.
